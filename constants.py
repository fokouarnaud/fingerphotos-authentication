from pathlib import Path

PATH_TO_ROOT = str(Path.cwd().parent)
IMAGES_PATH = PATH_TO_ROOT + '/dataset/WI'
MAX_FEATURES = 500
GOOD_MATCH_PERCENT = 0.25
DIST_THRESHOLD = 30
TRAIN_PER_CLASS = 6

